
import { chromium, Browser, BrowserContext, Page } from "@playwright/test";
import { Before, After, setDefaultTimeout } from "@cucumber/cucumber";



setDefaultTimeout(1000*60*2);
let browser: Browser
let browserContext: BrowserContext 
let page: Page

Before( async function(){
    browser = await chromium.launch({headless:false, channel:"chrome", args: ['--start-maximized']});
    browserContext = await browser.newContext({viewport:null, javaScriptEnabled:true,});
    page = await browserContext.newPage();
})


After( async function(){
    await page.close();
    await browserContext.close();
    await browser.close();
})

export {page};