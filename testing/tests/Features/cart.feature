Feature: Shopping on the website

  Scenario: Accessing the Laptops section
    Given I am on the website's homepage
    When I navigate to the laptops section
    And I add the selected laptop to the cart
    And I proceed to checkout
    Then the price displayed in the cart should match the laptop's price   
@skip
  Scenario: Add a laptop to the cart and skip price comparison
    Given I am on the website's homepage
    When I navigate to the laptops section
    And I add the selected laptop to the cart
    And I proceed to checkout
    Then I skip the price comparison



  

    