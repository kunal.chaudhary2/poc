import { Page } from 'playwright';
export default class LoginPage{
    page: Page;
    constructor(page:Page){
        this.page=page;

    }
    async gotoLoginPage(){
        await this.page.goto("https://tutorialsninja.com/demo/");
        await this.page.click('xpath=(//a[@title="My Account"])');
    }

    async loginToApp() {
    await this.page.click('xpath=//a[text()="Login"]');
    await this.page.fill('#input-email', 'jhon123@gmail.com');
    await this.page.fill('#input-password', '1234567890');
    await this.page.click('xpath=//input[@class="btn btn-primary"]');
    }

    async logout(){
        await this.page.locator("xpath=//a[contains(., 'Logout') and @class= 'list-group-item']").click();
        await this.page.locator("xpath=//a[contains(., 'Continue')]").click();
    }

}