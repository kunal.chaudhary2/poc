import { Given, When, Then } from '@cucumber/cucumber';
import {page} from '../../lib/basepage.spec'
import test, { expect } from '@playwright/test';
import LoginPage from '../pages/loginpage';

let loginPage:LoginPage

Given('I am on the website\'s homepage', async () => {
    loginPage = new LoginPage(page);
    await loginPage.gotoLoginPage();
    await loginPage.loginToApp();
});

When('I navigate to the laptops section', async () => {
    await page.click('xpath=//a[text()="Laptops & Notebooks"]');
    await page.click('xpath=//a[text()="Show AllLaptops & Notebooks"]');

});


When('I add the selected laptop to the cart', async () => {
    await page.click('xpath=(//span[text()="Add to Cart"])[3]');
    await page.click('xpath=(//span[text()="Add to Cart"])[4]');
    await page.click('xpath=//a[@title="Shopping Cart"]');
  

});

When('I proceed to checkout', async () => {
    const macBookAirText = await page.textContent('xpath=(//a[contains(@href,"product_id=44")])[4]');
    if (macBookAirText === 'MacBook Air') {
      console.log('Good to Go MAC AIR');
    } else {
      console.log('Not Air');
    }
  
    const macBookProText = await page.textContent('xpath=(//a[contains(@href,"product_id=45")])[4]');
    if (macBookProText === 'MacBook Pro') {
      console.log('Good to Go MAC PRO');
    } else {
      console.log('Not Pro');
    }

});


Then('the price displayed in the cart should match the laptop\'s price', async () => {
    const macBookAirText = await page.textContent('xpath=(//a[contains(@href,"product_id=44")])[4]');
    if (macBookAirText === 'MacBook Air') {
      console.log('Good to Go MAC AIR');
    } else {
      console.log('Not Air');
    }
  
    const macBookProText = await page.textContent('xpath=(//a[contains(@href,"product_id=45")])[4]');
    if (macBookProText === 'MacBook Pro') {
      console.log('Good to Go MAC PRO');
    } else {
      console.log('Not Pro');
    }
})

Then ("I skip the price comparison", async () => {
    console.log("Skipping price comparison")
})