
import { Given, When, Then, setDefaultTimeout, Before, After  } from "@cucumber/cucumber";
import { Browser, BrowserContext, Page, chromium } from '@playwright/test';
import {page} from '../../lib/basepage.spec'
import { expect } from '@playwright/test';
import LoginPage from '../pages/loginpage';
let loginPage:LoginPage


Given('User is on home page', async function () {
    // Write code here that turns the phrase above into concrete actions
   loginPage = new LoginPage(page);
    await loginPage.gotoLoginPage();
});
When('User enter login details', async function () {
    // Write code here that turns the phrase above into concrete actions
    await loginPage.loginToApp();
});
Then('Home should be displayed', async function () {
    // Write code here that turns the phrase above into concrete actions
    await page.locator("xpath=//a[contains(., 'Edit your account information')]").waitFor({timeout: 5000})
    const isVisible = await page.locator("xpath=//a[contains(., 'Edit your account information')]").isVisible();
     expect(isVisible).toEqual(true);
});
When('Upon logout', async function () {
    // Write code here that turns the phrase above into concrete actions
    await loginPage.logout();
});
When('Logout should be successfull', async function () {
    // Write code here that turns the phrase above into concrete actions
    console.log("User should be logged out")
});
